// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { BrowserRouter } from 'react-router-dom';
import NxWelcome from './nx-welcome';
import { CssBaseline } from '@mui/material';

export function App() {
  return (
      <BrowserRouter>
        <CssBaseline/>
        <NxWelcome/>
      </BrowserRouter>
  );
}

export default App;
