import * as React from 'react';
import { BrowserRouter, Link, Route, Routes } from 'react-router-dom';
import { CssBaseline } from '@mui/material';
import AppBar from '@mui/material/AppBar';
import Toolbar from '@mui/material/Toolbar';
import CameraIcon from '@mui/icons-material/PhotoCamera';
import Typography from '@mui/material/Typography';
import Box from '@mui/material/Box';
import Container from '@mui/material/Container';
import { createTheme, ThemeProvider } from '@mui/material/styles';
import { Footer } from '@cap/shared';
import { Suspense } from 'react';
const theme = createTheme();
const PeopleApp = React.lazy(() => import('people-app/App'));

const CarsApp = React.lazy(() => import('cars-app/App'));

export function App() {
  return (
    <BrowserRouter>
      <CssBaseline/>
      <ThemeProvider theme={theme}>
        <CssBaseline />
        <AppBar position="relative">
          <Toolbar>
            <CameraIcon sx={{ mr: 2 }} />
            <Typography variant="h6" color="inherit" noWrap>
              Cars and People
            </Typography>
          </Toolbar>
        </AppBar>
        <main>
          {/* Hero unit */}
          <Box
            sx={{
              bgcolor: 'background.paper',
              pt: 8,
              pb: 6,
            }}
          >
            <Container maxWidth="sm">
              <Typography
                component="h3"
                variant="h4"
                align="center"
                color="text.primary"
                gutterBottom
              >
                Welcome to Cars and People app
              </Typography>
              <Typography variant="h5" align="center" color="text.secondary" paragraph>
                This app connects cars and people. Got a person? Giv'em a car. Got a car? Assign it to people.
              </Typography>
              <Box
                sx={{
                  typography: 'body1',
                  '& > :not(style) + :not(style)': {
                    ml: 2,
                  },
                }}
              >
                <Link to="/people">People</Link>
                <Link to="/cars">Cars</Link>
              </Box>
            </Container>
          </Box>
          <Routes>
            <Route path="/people" element={ <Suspense fallback={<div>Loading...</div>}><PeopleApp/></Suspense>}>

            </Route>
            <Route path="/cars" element={<Suspense fallback={<div>Loading...</div>}><CarsApp/></Suspense>}></Route>
          </Routes>

        </main>
        <Footer/>
      </ThemeProvider>
    </BrowserRouter>
  );
}

export default App;
