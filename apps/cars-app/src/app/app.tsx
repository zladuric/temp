// eslint-disable-next-line @typescript-eslint/no-unused-vars
import styles from './app.module.scss';
import { CarList } from '@cap/cars';
import { CssBaseline } from '@mui/material';
import { BrowserRouter } from 'react-router-dom';
import Toolbar from '@mui/material/Toolbar';
import CameraIcon from '@mui/icons-material/PhotoCamera';
import Typography from '@mui/material/Typography';
import AppBar from '@mui/material/AppBar';
import * as React from 'react';
import { CarApp } from './CarApp';
import { Footer } from '@cap/shared';

export function App() {
  return (
    <BrowserRouter>
      <CssBaseline/>
      <AppBar position="relative">
        <Toolbar>
          <CameraIcon sx={{ mr: 2 }} />
          <Typography variant="h6" color="inherit" noWrap>
            Cars app
          </Typography>
        </Toolbar>
      </AppBar>
      <CarApp/>
      <Footer/>
    </BrowserRouter>
  );
}

export default App;
