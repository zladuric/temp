// eslint-disable-next-line @typescript-eslint/no-unused-vars
import styles from './app.module.scss';
import { CssBaseline } from '@mui/material';
import AppBar from '@mui/material/AppBar';
import Toolbar from '@mui/material/Toolbar';
import CameraIcon from '@mui/icons-material/PhotoCamera';
import Typography from '@mui/material/Typography';
import { BrowserRouter } from 'react-router-dom';
import * as React from 'react';

import { Footer } from '@cap/shared';
import { PeoplesApp } from './PeopleApp';

export function App() {
  return (
    <BrowserRouter>
      <CssBaseline/>
      <AppBar position="relative">
        <Toolbar>
          <CameraIcon sx={{ mr: 2 }} />
          <Typography variant="h6" color="inherit" noWrap>
            People app
          </Typography>
        </Toolbar>
      </AppBar>
      <PeoplesApp/>
      <Footer/>
    </BrowserRouter>
  );
}

export default App;
