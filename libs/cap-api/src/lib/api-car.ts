// We are saving things to local storage. It could also be an API.

import {Car} from "@cap/cap-shared";

const CARS_STORAGE_KEY = 'CaP::cars';

const defaultCars: Car[] = [
  {
    id: Math.floor(Math.random() * 1000),
    name: 'RAPHAEL MAKSIAN',
    carName: 'Red car',
    url: 'https://unsplash.com/@photograffihk',
    imageUrl: '/assets/img/red.jpg',
  },
  {
    id: Math.floor(Math.random() * 1000),
    name: 'Erik Odiin',
    carName: 'Blue car',
    url: 'https://unsplash.com/@odiin',
    imageUrl: '/assets/img/blue.jpg',

  },
  {
    id: Math.floor(Math.random() * 1000),
    name: 'Nick Gordon',
    carName: 'Green car',
    url: 'https://unsplash.com/@nick_g_pics',
    imageUrl: '/assets/img/green.jpg',
  },
  {
    id: Math.floor(Math.random() * 1000),
    name: 'Nick Baker',
    carName: 'Yellow car',
    url: 'https://unsplash.com/@nickb',
    imageUrl: '/assets/img/yellow.jpg',
  }
]

export function loadCars() {
  let cars: Car[] = [];
  try {
    const extraCars = JSON.parse(localStorage.getItem(CARS_STORAGE_KEY) as string) || [];
    cars = extraCars.concat(defaultCars);
  } catch {
    console.log('No cars to load.');
  }
  return cars;
}
