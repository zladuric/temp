// We are saving things to local storage. It could also be an API.

import {Person} from "@cap/cap-shared";

const PEOPLE_STORAGE_KEY = 'CaP::people';

export function loadPeople() {
  let people: Person[] = [];
  try {
    people = JSON.parse(localStorage.getItem(PEOPLE_STORAGE_KEY) as string) || [];
  } catch {
    console.log('No people to load.');
  }
  return people;
}

export function savePeople(people: Person[]) {
  localStorage.setItem(PEOPLE_STORAGE_KEY,  JSON.stringify(people));
}
