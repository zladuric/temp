export interface Car {
  id: number;
  name: string;
  carName: string;
  url: string;
  imageUrl: string;
}
