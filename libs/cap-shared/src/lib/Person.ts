import {Car} from "./Car";

export interface Person {
  id: number;
  name: string;
  car?: Car;
}
