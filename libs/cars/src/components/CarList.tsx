import useCars  from './useCars';
import { TreeItem, TreeView } from '@mui/lab';
import ExpandMoreIcon from '@mui/icons-material/ExpandMore';
import ChevronRightIcon from '@mui/icons-material/ChevronRight';
import Container from '@mui/material/Container';
import { useNavigate } from 'react-router-dom';
import { Person } from '@cap/cap-shared';

export function CarList() {
  const {
    cars,
  } = useCars();
  const navigate = useNavigate();

  const show = (person: Person) => {
    navigate(`/people?show=${person.id}`);
  }

  return <Container sx={ { py: 8 } } maxWidth="md">
    <TreeView
      aria-label="Car List"
      defaultCollapseIcon={<ExpandMoreIcon />}
      defaultExpandIcon={<ChevronRightIcon />}
      sx={{ height: 240, flexGrow: 1, maxWidth: 400, overflowY: 'auto' }}
    >
      {cars.map(car => <TreeItem nodeId={ `${ car.id }` }
                                 label={ `${car.carName} (${car.owners.length})` }>
        {car.owners && car.owners.length > 0 && car.owners.map(owner =>
          <TreeItem nodeId={` ${owner.id}`} label={owner.name} onClick={() => show(owner)}/>
        )}
      </TreeItem>)}
    </TreeView>
  </Container>
}

export default CarList
