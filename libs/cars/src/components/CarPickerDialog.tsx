import * as React from 'react';
import {useState} from 'react';
import DialogTitle from '@mui/material/DialogTitle';
import Dialog from '@mui/material/Dialog';
import {ImageList, ImageListItem} from '@mui/material';
import {Car} from "@cap/cap-shared";
import {loadCars} from "@cap/cap-api";


export function CarPickerDialog(props: {
  pick: (car: Car) => void,
  open: boolean,
  handleClose: () => void
}) {
  const {pick, open, handleClose} = props;
  const [name, setName] = useState<string>('');
  const cars = loadCars();

  const handleClick = (car: Car) => {
    pick(car);
    handleClose();
  };

  return (
    <Dialog onClose={handleClose} open={open}>
      <DialogTitle>Add a person</DialogTitle>
      <ImageList sx={{width: 500, height: 450}} cols={3} rowHeight={164}>
        {cars.map(car => <ImageListItem key={car.id}
                                        onClick={() => handleClick(car)}
          >
            <img
              src={`${car.imageUrl}?w=164&h=164&fit=crop&auto=format`}
              alt={car.name}
              loading="lazy"
            />
          </ImageListItem>,
        )}
      </ImageList>
    </Dialog>
  );
}

