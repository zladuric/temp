import { useEffect, useState } from 'react';
import {Car, Person} from "@cap/cap-shared";
import {loadCars, loadPeople} from '@cap/cap-api';

export interface CarWithPeople extends Car {
  owners: Person[];
}

export default function useCars() {
  const [cars, setCars] = useState<CarWithPeople[]>([]);

  useEffect(() => {
    const items = loadCars()
      .map(item => ({
        ...item,
          owners: getCarOwners(item)
      }));
    setCars(items);
  }, [])

  const getCarOwners = (car: Car) => {
    const people = loadPeople();
    return people.filter(person => person.car && person.car.carName === car.carName);
  }

  return {
    cars
  }
}
