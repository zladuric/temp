import * as React from 'react';
import DialogTitle from '@mui/material/DialogTitle';
import Dialog from '@mui/material/Dialog';
import { useContext, useState } from 'react';
import { TextField } from '@mui/material';
import Box from '@mui/material/Box';
import Button from '@mui/material/Button';
import { PeopleContext } from '../context/PeopleProvider';


export interface AddPersonDialogProps {
  open: boolean;
  onClose: () => void;
}

export default function AddPersonDialog(props: AddPersonDialogProps) {
  const [name, setName] = useState<string>('');
  const { onClose, open } = props;

  const { addPerson } = useContext(PeopleContext);

  const onChange = (e: any) => setName(e.target.value);

  const handleClose = () => {
    if (name) {
      addPerson(name);
    }
    onClose();
  };

  const handleSubmit = (event: React.FormEvent<HTMLFormElement>) => {
    event.preventDefault();
    const data = new FormData(event.currentTarget);
    const val = data.get('name') as string;
    addPerson(val);
    onClose();
  };


  return (
    <Dialog onClose={handleClose} open={open}>
      <DialogTitle>Add a person</DialogTitle>
      <Box
        component="form"
        sx={{
          '& .MuiTextField-root': { m: 1, width: '25ch' },
          p: '1em'
        }}
        noValidate
        autoComplete="off"
        onSubmit={handleSubmit}
      >

        <TextField id="name" name="name" label="Name" variant="standard"
                   onChange={onChange}
        />
        <br/>
        <Button
          type="submit"
          variant="contained"
          sx={{ mt: 3, mb: 2 }}
        >
          Add person
        </Button>
        <Button
          sx={{ mt: 3, mb: 2 }}
          onClick={() => onClose()}
        >
          Cancel
        </Button>
      </Box>
    </Dialog>
  );
}
