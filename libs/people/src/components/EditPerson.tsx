import * as React from 'react';
import { useContext, useState } from 'react';
import DialogTitle from '@mui/material/DialogTitle';
import Dialog from '@mui/material/Dialog';
import Button from '@mui/material/Button';
import { PeopleContext } from '../context/PeopleProvider';
import {  CarPickerDialog } from '@cap/cars';
import {Car, Person} from "@cap/cap-shared";

interface EditPersonProps {
  person: Person;
}

export default function EditPerson({ person }: EditPersonProps) {
  const [open, setOpen] = useState(false);
  const { addCarToPerson } = useContext(PeopleContext);

  const handleClose = () => {
    setOpen(false);
  };
  const pickAnother = (car: Car) => {
    addCarToPerson(person, car);
    setOpen(false);
  };

  return <>
    <Button size="small" onClick={ () => setOpen(true) }>Change car</Button>
    <EditPersonDialog
      open={ open }
      onClose={ handleClose }
      pick={ pickAnother }
    ></EditPersonDialog>
  </>;
}


interface EditPersonDialogProps {
  open: boolean;
  onClose: () => void;
  pick: (car: Car) => void;
}

export function EditPersonDialog(props: EditPersonDialogProps) {
  const { onClose, open, pick } = props;
  return (
    <Dialog onClose={ onClose } open={ open }>
      <DialogTitle>Pick another car</DialogTitle>
      <CarPickerDialog
        open={ open }
        handleClose={ onClose }
        pick={ pick }
      ></CarPickerDialog>
    </Dialog>
  );
}
