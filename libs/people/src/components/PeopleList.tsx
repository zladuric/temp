import { Car } from '@cap/cap-shared';
import Grid from '@mui/material/Grid';
import * as React from 'react';
import { useContext } from 'react';
import { PeopleContext } from '../context/PeopleProvider';
import { PersonCard } from './PersonCard';

export interface PeopleListProps {
  cars: Car[];
}

export default function PeopleList({ cars }: PeopleListProps) {
  const { people } = useContext(PeopleContext);
  if (!people) {
    return <>Loading...</>;
  } else if (people.length === 0) {
    return <>No people to show.</>;
  }
  return <Grid container spacing={ 4 }>
    { people.map((person) => (
      <Grid item key={ person.id } xs={ 12 } sm={ 6 } md={ 4 }>
        <PersonCard person={ person }></PersonCard>
      </Grid>
    )) }
  </Grid>;
}
