import CardMedia from '@mui/material/CardMedia';
import CardContent from '@mui/material/CardContent';
import Typography from '@mui/material/Typography';
import CardActions from '@mui/material/CardActions';
import Button from '@mui/material/Button';
import Card from '@mui/material/Card';
import * as React from 'react';
import { useContext, useEffect, useState } from 'react';
import { ImageList, ImageListItem } from '@mui/material';
import Box from '@mui/material/Box';
import Link from '@mui/material/Link';
import PickCar from './PickCar';
import EditPerson from './EditPerson';
import { PeopleContext } from '../context/PeopleProvider';
import { useSearchParams } from 'react-router-dom';
import {Person} from "@cap/cap-shared";

export function PersonCard({ person }: { person: Person }) {
  const [searchParams] = useSearchParams();
  const [isShown, setIsShown] = useState(false);


  useEffect(() => {
    const show = searchParams.get('show');
    console.log('Search params', show);
    if (show === `${ person.id }`) {
      setIsShown(true);
    } else {
      setIsShown(false);
    }
  }, [searchParams]);

  const { deletePerson } = useContext(PeopleContext);
  return <Card
    raised={ isShown }
    sx={ { height: '100%', display: 'flex', flexDirection: 'column' } }
  >
    <CardMedia
      component="img"
      sx={ {
        maxWidth: 300,
        maxHeight: 300,
        objectFit: 'contain',
      } }

      image="/assets/img/avatar.jpg"
      alt="random"
    />
    <CardContent sx={ { flexGrow: 1 } }>
      <Typography gutterBottom variant="h5" component="h2">
        { person.name }
      </Typography>
      { !person.car && <PickCar person={ person }/> }


      { person.car && <Box>
        <ImageList cols={ 1 } rowHeight={ 164 }>
          <ImageListItem key={ person.car.id }>
            <img
              src={ `${ person.car.imageUrl }?w=164&h=124&fit=crop&auto=format` }
              alt={ person.car.name }
              loading="lazy"
            />
          </ImageListItem>
        </ImageList>
        { person.car && <Typography>
          Picture taken by <Link href={ person.car.url }>{ person.car.name }</Link> on Unsplash.
        </Typography> }
      </Box> }
    </CardContent>
    <CardActions>
      <Button size="small" disabled>View</Button>
      { person.car && <Typography>
        <EditPerson person={ person }/>
      </Typography> }
      <Button size="small" onClick={ () => deletePerson(person) }>Delete</Button>
    </CardActions>
  </Card>;
}
