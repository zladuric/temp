import Typography from '@mui/material/Typography';
import Button from '@mui/material/Button';
import { useContext, useState } from 'react';
import { PeopleContext } from '../context/PeopleProvider';
import { CarPickerDialog } from '@cap/cars';
import {Car, Person} from "@cap/cap-shared";

interface PickerProps {
  person: Person,
}

export default function PickCar({ person }: PickerProps) {
  const [open, setOpen] = useState(false);
  const { addCarToPerson } = useContext(PeopleContext);
  const handleClose = () => setOpen(false);
  const pickCar = (car: Car) => {
    addCarToPerson(person, car);
    setOpen(false);
  };
  return <Typography gutterBottom variant="h5" component="h2">
    <Button variant="outlined" onClick={ () => setOpen(true) }>Pick a car</Button>
    <CarPickerDialog
      open={ open }
      handleClose={ handleClose }
      pick={ pickCar }
    ></CarPickerDialog>
  </Typography>;
}
