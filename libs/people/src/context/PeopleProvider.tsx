import { createContext, ReactNode } from 'react';
import usePeople from './usePeople';
import { Car, Person } from '@cap/cap-shared';

interface PeopleContextInterface {
  people: Person[],
  addPerson: (name: string) => void,
  deletePerson: (person: Person) => void,
  addCarToPerson: (person: Person, car: Car) => void,
}

export const PeopleContext = createContext<PeopleContextInterface>({
  people: [],
  addPerson: (_: string) => undefined,
  deletePerson: (_: Person) => undefined,
  addCarToPerson: (person: Person, car: Car) => undefined,
})

export default function PeopleProvider({children}: { children: ReactNode }) {
  const values = usePeople();
  return <PeopleContext.Provider value={ values }>{children}</PeopleContext.Provider>
}
