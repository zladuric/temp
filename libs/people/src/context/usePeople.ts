import {Car, Person} from '@cap/cap-shared';
import { useEffect, useState } from 'react';
import { useIsFirstRender } from 'usehooks-ts';
import {loadPeople, savePeople} from "@cap/cap-api";

export default function usePeople() {
  const isFirst = useIsFirstRender();
  const [people, setPeople] = useState<Person[]>([]);

  useEffect(() => {
    const storedPeople = loadPeople();
    setPeople(storedPeople);
  }, [isFirst]);

  const addPerson = (name?: string) => {
    if (!name) {
      return;
    }
    // good random number generator, strong crypto
    const id = Math.ceil(Math.random() * 1000);
    const person: Person = {
      id,
      name,
    };

    const newPeople = people.concat(person);
    savePeople(newPeople);
    setPeople(newPeople);
  };

  const addCarToPerson = (person: Person, car: Car) => {
    person.car = car;
    const newPeople = people.map(p => p.id === person.id ? person : p);
    savePeople(newPeople);
    setPeople(newPeople);
  };

  const deletePerson = (person: Person) => {
    const newPeople = people.filter(p => p.id !== person.id);
    savePeople(newPeople);
    setPeople(newPeople);
  };

  return {
    people,
    addPerson,
    addCarToPerson,
    deletePerson,
  };
}
