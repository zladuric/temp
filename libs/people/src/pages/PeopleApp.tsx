import PeopleProvider from '../context/PeopleProvider';
import Container from '@mui/material/Container';
import * as React from 'react';
import { useState } from 'react';
import PeopleList from '../components/PeopleList';
import Button from '@mui/material/Button';
import Typography from '@mui/material/Typography';
import AddPersonDialog from '../components/AddPersonDialog';
import { Car } from '@cap/cap-shared';

export function PeopleApp({ cars }: { cars: Car[] }) {
  const [open, setOpen] = useState(false);

  const handleCloseDialog = () => {
    setOpen(false);
  };
  return <>
    <PeopleProvider>
      <Typography variant="subtitle1" component="div" sx={ { px: 2 } }>
        <Button variant="outlined" onClick={ () => setOpen(true) }>Add person</Button>
        <AddPersonDialog
          open={ open }
          onClose={ handleCloseDialog }
        />
      </Typography>
      <Container sx={ { py: 8 } } maxWidth="md">
        <PeopleList cars={ cars }></PeopleList>
      </Container>
    </PeopleProvider>
  </>;
};
