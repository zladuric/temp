import Typography from '@mui/material/Typography';
import Box from '@mui/material/Box';
import * as React from 'react';
import Link from '@mui/material/Link';

function Copyright() {
  return (
    <Typography variant="body2" color="text.secondary" align="center">
      {'Copyright © '}
      <Link color="inherit" href="https://zlatko.dev/">
        Zlatko
      </Link>{' '}
      {new Date().getFullYear()}
      {'.'}
    </Typography>
  );
}

export function Footer() {
  return <Box sx={{ bgcolor: 'background.paper', p: 6 }} component="footer">
    <Typography variant="h6" align="center" gutterBottom>
      Footer
    </Typography>
    <Typography
      variant="subtitle1"
      align="center"
      color="text.secondary"
      component="p"
    >
      I am shamelessly borrowing a lot of this code from various sources, see README.
    </Typography>
    <Copyright />
  </Box>
}
